﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class TradeGood
    {

        public int GoodID { get; set; }
        public string GoodName { get; set; }
        public int BuyersPay { get; set; }
        public int SellersGet { get; set; }
        public string GoodImage { get; set; }

        public static List<TradeGood> DefaultTradeGoodsList = new List<TradeGood>
            {
                new TradeGood { GoodID = 0,  GoodName="None"        , GoodImage = "Resources/resources/none.png"  },
                new TradeGood { GoodID = 1,  GoodName="Wheat"       , GoodImage = "Resources/resources/wheat.png"  },
                new TradeGood { GoodID = 2,  GoodName="Vegetables"  , GoodImage = "Resources/resources/Vegetables.png"  },
                new TradeGood { GoodID = 3,  GoodName="Fruits"      , GoodImage = "Resources/resources/Fruits.png"  },
                new TradeGood { GoodID = 4,  GoodName="Olives"      , GoodImage = "Resources/resources/Olives.png"  },
                new TradeGood { GoodID = 5,  GoodName="Vines"       , GoodImage = "Resources/resources/Vines.png"  },
                new TradeGood { GoodID = 6,  GoodName="Meat"        , GoodImage = "Resources/resources/Meat.png"  },
                new TradeGood { GoodID = 7,  GoodName="Wine"        , GoodImage = "Resources/resources/wine.png"  },
                new TradeGood { GoodID = 8,  GoodName="Oil"         , GoodImage = "Resources/resources/oil.png"  },
                new TradeGood { GoodID = 9,  GoodName="Iron"        , GoodImage = "Resources/resources/iron.png"  },
                new TradeGood { GoodID = 10, GoodName="Timber"      , GoodImage = "Resources/resources/timber.png"  },
                new TradeGood { GoodID = 11, GoodName="Clay"        , GoodImage = "Resources/resources/clay.png"  },
                new TradeGood { GoodID = 12, GoodName="Marble"      , GoodImage = "Resources/resources/marble.png"  },
                new TradeGood { GoodID = 13, GoodName="Weapons"     , GoodImage = "Resources/resources/weapons.png"  },
                new TradeGood { GoodID = 14, GoodName="Furniture"   , GoodImage = "Resources/resources/furniture.png"  },
                new TradeGood { GoodID = 15, GoodName="Pottery"     , GoodImage = "Resources/resources/pottery.png"  },
            };


        /// <summary>
        /// Reads the goods prices from the save data for both buyers and sellers
        /// </summary>
        /// <param name="br"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        /// 
        //TODO remember why the need for listType??? refactor?
        public static List<int> GetPriceList (BinaryReader br, string listType)
        {

            int startOffset = 1203260;
            int valueLen = 4;

            List<int> BuyPrices  = new List<int>();
            List<int> SellPrices = new List<int>();
            for (int i = 0; i < TradeGood.DefaultTradeGoodsList.Count; i++)
            {
                int offset = startOffset + i * valueLen * 2;
                //SellPrices.Add   (HexReadWrite.ReadHexInt(br, offset + valueLen, valueLen));
                //BuyPrices.Add    (HexReadWrite.ReadHexInt(br, offset, valueLen));

                SellPrices.Add(BinaryRW.ReadInt32(offset + 4));
                BuyPrices.Add(BinaryRW.ReadInt32(offset));
            }

            return (listType == "buyPrices") ? BuyPrices : SellPrices;
        }




        public static void GetPriceList()
        {
            //List<TradeGood> tradeGoods = TradeGood.DefaultTradeGoodsList;
            int startOffset = 1203260;

            foreach (TradeGood tradeGood in DefaultTradeGoodsList)
            {
                tradeGood.SellersGet = BinaryRW.ReadInt32(startOffset + tradeGood.GoodID * 8);
                tradeGood.BuyersPay = BinaryRW.ReadInt32(startOffset + tradeGood.GoodID * 8 + 4);

            }

            //return tradeGoods;
        }


        public static void SetPriceList(ObservableCollection<TradeGood> TradeGoodsList)
        {

            int startOffset = 1203260;

            foreach (TradeGood tradeGood in TradeGoodsList)
            {
                BinaryRW.WriteInt32(startOffset + tradeGood.GoodID * 8, tradeGood.SellersGet);
                BinaryRW.WriteInt32(startOffset + tradeGood.GoodID * 8 + 4, tradeGood.BuyersPay);

            }

        }
    }
}
