﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    public static class BinaryRW
    {
        #region custom methods

        public static string ReadGovernorName() => System.Text.RegularExpressions.Regex.Replace(ReadString(944182, 70), "[^A-Za-z0-9 ]", "");
        public static void WriteGovernorName(string governorName)
        {
            //int governorNameLength = governorName.Length;
            for (int i = 244182; i < 244182 + 71; i++) WriteByte(i, 0);
            WriteString(244182, governorName);
        }
        
        #endregion


        #region Read Values Methods

        public static int ReadByte(int offset)
        {
            MainWindow.br.BaseStream.Position = offset;
            return int.Parse(MainWindow.br.ReadByte().ToString("X2"), System.Globalization.NumberStyles.HexNumber);
        }
        public static int ReadInt16(int offset)
        {
            MainWindow.br.BaseStream.Position = offset;
            return (int)MainWindow.br.ReadInt16();
        }
        public static int ReadInt32(int offset)
        {
            MainWindow.br.BaseStream.Position = offset;
            return (int)MainWindow.br.ReadInt32();
        }
        public static string ReadString(int offset, int stringLen)
        {
            MainWindow.br.BaseStream.Position = offset;
            return new string (MainWindow.br.ReadChars(stringLen));
        }
        public static bool ReadBool(int offset)
        {
            MainWindow.br.BaseStream.Position = offset;
            return MainWindow.br.ReadBoolean();
        }

        #endregion


        #region Write Values Methods

        public static void WriteByte(int offset, byte writeByte)
        {
            MainWindow.bw.BaseStream.Position = offset;
            MainWindow.bw.Write(writeByte);
        }
        public static void WriteInt16(int offset, int writeInt16)
        {
            MainWindow.bw.BaseStream.Position = offset;
            MainWindow.bw.Write((short)writeInt16);
        }
        public static void WriteInt32(int offset, int writeInt32)
        {
            MainWindow.bw.BaseStream.Position = offset;
            MainWindow.bw.Write(writeInt32);
        }
        public static void WriteString(int offset, string writeString)
        {
            MainWindow.bw.BaseStream.Position = offset;
            MainWindow.bw.Write(writeString);
        }
        public static void WriteBool(int offset, bool writeBool)
        {
            MainWindow.bw.BaseStream.Position = offset;
            MainWindow.bw.Write(writeBool);
        }

        #endregion

    }
}
