﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class MapCity : PropertyChangedBase
    {

        #region static lists
        public static readonly List<string> cityNames = new List<string>()
        {
            "Roma",
            "Tarentum",
            "Capua",
            "Brundisium",
            "Mediolanum",
            "Carthago Nova",
            "Carthago",
            "Tarraco",
            "Athenae",
            "Pergamum",
            "Syracuse",
            "Toletum",
            "Tarsus",
            "Leptis Magna",
            "Tingis",
            "Corinthus",
            "Valentia",
            "Ephesus",
            "Miletus",
            "Sinope",
            "Cyrene",
            "Antioch",
            "Heliopolis",
            "Damascus",
            "Hierosolyma",
            "Lindum",
            "Calleva",
            "Lutetia",
            "Massilia",
            "Narbo",
            "Lugdunum",
            "Caesarea",
            "Alexandria",
            "Augusta Trevorum",
            "Argentoratum",
            "Volubilis",
            "Londinium",
            "Thamugadi",
            "Sarmizegetusa",
            "Byzantium",

        };
        public static readonly List<string> cityTypes = new List<string>()
        {
            "A Roman City",
            "Our city!",
            "Trade Route",
            "Trade Route",
            "A distant city",
            "A Roman City"
        };
        public static List<int> QuotasList = new List<int>(TradeGood.DefaultTradeGoodsList.Count);
        #endregion


        #region private properties
        bool isCityActive;
        int cityTypeID;
        int cityNameId = 0;
        int quotaOffset;
        bool isTradeOpened = false;
        List<bool> buysFlagsList = new List<bool>(16);
        List<bool> sellsFlagsList = new List<bool>(16);
        int openTradeCost;
        int empireObjID;
        bool isSeaRoute;
        #endregion

        
        public int OpenTradeCost
        {
            get => openTradeCost;
            set
            {
                if (openTradeCost != value)
                {
                    openTradeCost = value;
                    RaisePropertyChanged("OpenTradeCost");
                }
            }
        }
        public int CityTypeId
        {
            get => cityTypeID; 
            set
            {
                if (cityTypeID!=value)
                {
                    cityTypeID = value;
                    RaisePropertyChanged("CityTypeId");
                }
            }
        }
        public int CityNameId
        {
            get => cityNameId; 
            set
            {
                if (cityNameId!=value)
                {
                    cityNameId = value;
                    RaisePropertyChanged("CityNameId");
                }
            }
        }
        public int QuotaOffset
        {
            get => quotaOffset; 
            set
            {
                if (quotaOffset!=value)
                {
                    quotaOffset = value;
                    RaisePropertyChanged("QuotaOffset");
                }
            }
        }



        #region methods
        public static List<MapCity> GetTradeCityList(List<MapCity> mapCitiesList)
        {
            List<MapCity> tradeCitiesList = new List<MapCity>();
            foreach (MapCity mapCity in mapCitiesList) if (mapCity.cityTypeID == 2) tradeCitiesList.Add(mapCity);
            return tradeCitiesList;
        }

        public ObservableCollection<MapCity> NewTradeCityList(BinaryReader br) => new ObservableCollection<MapCity>(GetMapCityList(br).Where(x => x.cityTypeID == 2).ToList());

        public static List<MapCity> GetMapCityList(BinaryReader br)
        {
            List<MapCity> mapCitiesList = new List<MapCity>();
            for (int j = 0; j < cityNames.Capacity; j++)
            {
                int offset = 1200492 + j * 66;

                List<bool> buysList = new List<bool>(16);
                List<bool> sellsList = new List<bool>(16);
                for (int i = 0; i < 16; i++)
                {
                    buysList.Add(HexReadWrite.ReadHexInt(br, offset +  6 + i, 1) != 0 ? true : false);
                }
                for (int i = 0; i < 16; i++)
                {
                    sellsList.Add(HexReadWrite.ReadHexInt(br, offset + 22 + i, 1) != 0 ? true : false);
                }
                mapCitiesList.Add(new MapCity
                {
                    isCityActive    = HexReadWrite.ReadHexInt(br, offset,      1) != 0 ? true : false,
                    CityTypeId      = HexReadWrite.ReadHexInt(br, offset + 2,  1),
                    CityNameId      = HexReadWrite.ReadHexInt(br, offset + 3,  1),
                    QuotaOffset     = HexReadWrite.ReadHexInt(br, offset + 4,  1),
                    isTradeOpened   = HexReadWrite.ReadHexInt(br, offset + 5,  1) == 1 ? true : false,
                    buysFlagsList   = buysList,
                    sellsFlagsList  = sellsList,
                    openTradeCost   = HexReadWrite.ReadHexInt(br, offset + 38, 2),
                    empireObjID     = HexReadWrite.ReadHexInt(br, offset + 46, 1),
                    isSeaRoute      = HexReadWrite.ReadHexInt(br, offset + 48, 1) == 1 ? true : false,

                });
            }
            List<MapCity> newlist = new List<MapCity>(mapCitiesList.OrderBy(o => o.empireObjID).ToList());
            return newlist;
        }
        #endregion





        #region temporary
        public static string GetTradeCity(BinaryReader br, int offset)
        {
            string tradeCity = "";
            for (int i = offset; i < offset + 66; i++)
            {
                tradeCity += $"{HexReadWrite.ReadHexInt(br, i, 1)}\t";
            }
            return $"{tradeCity}\n";
        }
        public static string GetTradeQuota(BinaryReader br, int offset)
        {
            string tradeQuota = "";
            for (int i = 0; i <  16; i++)
            {
                tradeQuota += $"{HexReadWrite.ReadHexInt(br, offset + i * 4, 4)}\t";
            }
            return $"{tradeQuota}\n";
        }
        #endregion



        //TODO SetTradeCity

        //TODO SetTradeQuota

    }
}
