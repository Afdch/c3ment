﻿using System;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    internal class HexReadWrite
    {        
        /// <summary>
        /// Reads hex values starting from startOffset address for dataLen length
        /// in backwards order and converts it to integer
        /// </summary>
        /// <param name="br"><see cref="BinaryReader"/> to read from</param>
        /// <param name="startOffset">Starting address</param>
        /// <param name="dataLen">value length</param>
        /// <returns><see cref="int"/></returns>    
        public static int ReadHexInt(BinaryReader br, int startOffset, int dataLen)
        {
            string hexString = "";
            for (int i = startOffset + dataLen - 1; i >= startOffset; i--)
            {
                br.BaseStream.Position = i;
                hexString += br.ReadByte().ToString("X2");
            }
            return int.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
        }

        /// <summary>
        /// Reads 2 bytes and converts them into a signed <see cref="Int16"/> value
        /// </summary>
        /// <param name="startOffset">The starting position to read, little indean</param>
        /// <returns>Signed <see cref="Int16"/></returns>
        public static int ReadHexInt16 (int startOffset)
        {
            string hexString = "";
            BinaryReader br = MainWindow.br;
            br.BaseStream.Position = (startOffset + 1);
            hexString += br.ReadByte().ToString("X2");
            br.BaseStream.Position = (startOffset);
            hexString += br.ReadByte().ToString("X2");

            return (int)Convert.ToInt16(hexString, 16);
        }


        public static string ReadGovernorName()
        {
            MainWindow.br.BaseStream.Position = 944182;
            return System.Text.RegularExpressions.Regex.Replace(new string(MainWindow.br.ReadChars(70)), "[^A-Za-z0-9 ]", "");
        }

        public static string ReadHexString(int startOffset, int stringLen)
        {
            string hexSting = "";
            for (int i = startOffset; i < startOffset + stringLen; i++)
            {

                //this converts the string back to hex
                //hexSting += ReadHexInt(MainWindow.br, i, 1).ToString("X");

                hexSting += (char)ReadHexInt(MainWindow.br, i, 1);

                
            }

            return hexSting;
        }



        /// <summary>
        /// Reads hex value at startOffset address and converts it to boolean
        /// </summary>
        /// <param name="br">BinaryReader to read from</param>
        /// <param name="startOffset">Starting address</param>
        /// <returns></returns>
        public bool ReadHexBool(BinaryReader br, int startOffset)
        {
            
            br.BaseStream.Position = startOffset;
            return br.ReadByte().ToString("X2") == "00" ? false : true;
        }

        /// <summary>
        /// I want to kill myself because of this function
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataLen"></param>
        /// <returns></returns>
        public static string IntToHex (int data, int dataLen)
        {
            char[] charArray = data.ToString($"X{dataLen * 2}").ToCharArray();
            
            char[] charArray2 = new char[dataLen * 2];
            for (int i = 0; i < dataLen; i++)
            {
                charArray2[i * 2]       = charArray[dataLen *2 - (i + 1) * 2];
                charArray2[i * 2 + 1]   = charArray[dataLen *2 - (i + 1) * 2 + 1];
            }

            return new string(charArray2);
        }

        public static byte[] IntToByteArray(int value, int dataLen)
        {
            byte[] array = BitConverter.GetBytes(value);
            //Array.Reverse(array);
            Array.Resize(ref array, dataLen);

            return array;
        }

        public static void WriteHexInt(BinaryWriter bw, int startOffset, int dataLen, int data)
        {
            bw.BaseStream.Position = startOffset;
            bw.Write(IntToHex(data, dataLen));

        }

        public static void WriteHexBool(BinaryWriter bw, int startOffset, bool value)
        {
            bw.BaseStream.Position = startOffset;
            bw.Write(IntToHex(value ? 1 : 0, 1));
        }

    }
}