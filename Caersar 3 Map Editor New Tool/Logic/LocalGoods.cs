﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class LocalGood
    {
        public string LocalGoodName { get; set; }
        public bool IsAllowed { get; set; }


        public static LocalGood GetBuildingsAvailability(BinaryReader br, TradeGood tradeGood)
            => new LocalGood
            {
                IsAllowed = HexReadWrite.ReadHexInt(br, 1205006 + 2 * tradeGood.GoodID, 2) == 1,
                LocalGoodName = tradeGood.GoodName
            };

    }
}
