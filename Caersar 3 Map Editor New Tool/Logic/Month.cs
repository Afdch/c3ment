﻿using System.Collections.Generic;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class Month
    {

        public int MonthId { get; private set; }
        public string MonthName { get; private set; }

        public static readonly List<Month> Months = new List<Month>
        {
            new Month { MonthId = 0, MonthName = "January"},
            new Month { MonthId = 1, MonthName = "February"},
            new Month { MonthId = 2, MonthName = "March"},
            new Month { MonthId = 3, MonthName = "April"},
            new Month { MonthId = 4, MonthName = "May"},
            new Month { MonthId = 5, MonthName = "June"},
            new Month { MonthId = 6, MonthName = "July"},
            new Month { MonthId = 7, MonthName = "August"},
            new Month { MonthId = 8, MonthName = "September"},
            new Month { MonthId = 9, MonthName = "October"},
            new Month { MonthId = 10, MonthName = "November"},
            new Month { MonthId = 11, MonthName = "December"},
        };
    }
}
