﻿using System.Collections.Generic;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class Building
    {
        public int BuildingID { get; set; }
        public string BuildingName { get; set; }
        public bool IsAllowed { get; set; }

        /// <summary>
        /// stores the list for every buidling
        /// </summary>
        public static List<Building> allowedBuildingsList = new List<Building>
        {
            new Building {BuildingID = 0, BuildingName = "Farms", IsAllowed = false},
            new Building {BuildingID = 1, BuildingName = "Raw resourses", IsAllowed = false},
            new Building {BuildingID = 2, BuildingName = "Workshops", IsAllowed = false},
            new Building {BuildingID = 3, BuildingName = "Road", IsAllowed = false},
            new Building {BuildingID = 4, BuildingName = "Wall", IsAllowed = false},
            new Building {BuildingID = 5, BuildingName = "Aqueduct", IsAllowed = false},
            new Building {BuildingID = 6, BuildingName = "Housing", IsAllowed = false},
            new Building {BuildingID = 7, BuildingName = "Amphitheater", IsAllowed = false},
            new Building {BuildingID = 8, BuildingName = "Theater", IsAllowed = false},
            new Building {BuildingID = 9, BuildingName = "Hippodrome", IsAllowed = false},
            new Building {BuildingID = 10, BuildingName = "Colosseum", IsAllowed = false},
            new Building {BuildingID = 11, BuildingName = "Gladiator School", IsAllowed = false},
            new Building {BuildingID = 12, BuildingName = "Lion Pit", IsAllowed = false},
            new Building {BuildingID = 13, BuildingName = "Actor Colony", IsAllowed = false},
            new Building {BuildingID = 14, BuildingName = "Chariot Maker", IsAllowed = false},

            new Building {BuildingID = 15, BuildingName = "Gardens", IsAllowed = false},
            new Building {BuildingID = 16, BuildingName = "Plaza", IsAllowed = false},
            new Building {BuildingID = 17, BuildingName = "Statues", IsAllowed = false},
            new Building {BuildingID = 18, BuildingName = "Clinic", IsAllowed = false},
            new Building {BuildingID = 19, BuildingName = "Hospital", IsAllowed = false},
            new Building {BuildingID = 20, BuildingName = "Bath-house", IsAllowed = false},
            new Building {BuildingID = 21, BuildingName = "Barber", IsAllowed = false},
            new Building {BuildingID = 22, BuildingName = "School", IsAllowed = false},
            new Building {BuildingID = 23, BuildingName = "Academy", IsAllowed = false},
            new Building {BuildingID = 24, BuildingName = "Library", IsAllowed = false},
            new Building {BuildingID = 25, BuildingName = "Prefecture", IsAllowed = false},

            new Building {BuildingID = 26, BuildingName = "Fort", IsAllowed = false},
            new Building {BuildingID = 27, BuildingName = "Gatehouse", IsAllowed = false},
            new Building {BuildingID = 28, BuildingName = "Tower", IsAllowed = false},
            new Building {BuildingID = 29, BuildingName = "Small Temple", IsAllowed = false},
            new Building {BuildingID = 30, BuildingName = "Large Temple", IsAllowed = false},
            new Building {BuildingID = 31, BuildingName = "Market", IsAllowed = false},
            new Building {BuildingID = 32, BuildingName = "Granary", IsAllowed = false},
            new Building {BuildingID = 33, BuildingName = "Warehouse", IsAllowed = false},
            new Building {BuildingID = 34, BuildingName = "Triumphal Arch", IsAllowed = false},
            new Building {BuildingID = 35, BuildingName = "Dock", IsAllowed = false},
            new Building {BuildingID = 36, BuildingName = "Wharf + Shipyard", IsAllowed = false},
            new Building {BuildingID = 37, BuildingName = "Governor'S Palace", IsAllowed = false},
            new Building {BuildingID = 38, BuildingName = "Engineer's post", IsAllowed = false},
            new Building {BuildingID = 39, BuildingName = "Senate", IsAllowed = false},
            new Building {BuildingID = 40, BuildingName = "Forum", IsAllowed = false},
            new Building {BuildingID = 41, BuildingName = "Well", IsAllowed = false},
            new Building {BuildingID = 42, BuildingName = "Oracle", IsAllowed = false},
            new Building {BuildingID = 43, BuildingName = "Mission Post", IsAllowed = false},
            new Building {BuildingID = 44, BuildingName = "Bridges", IsAllowed = false},
            new Building {BuildingID = 45, BuildingName = "Barracks", IsAllowed = false},
            new Building {BuildingID = 46, BuildingName = "Military Academy", IsAllowed = false},
        };


        /// <summary>
        /// Reads values for every building
        /// starting offset is 125006, int16 format
        /// </summary>
        public static void GetBuildingsAvailability()
        {
            foreach (Building building in allowedBuildingsList)
            {
                
                building.IsAllowed = BinaryRW.ReadInt16(1205006 + building.BuildingID * 2) == 1;
            }
        }


        /// <summary>
        /// Writes values for every building
        /// starting offset is 125006, int16 format
        /// </summary>
        public static void SetBuildingsAvailability()
        {
            foreach (Building building in allowedBuildingsList)
            {
                BinaryRW.WriteInt16(1205006 + building.BuildingID * 2, building.IsAllowed ? 1 : 0);
            }
        }



    }
}
