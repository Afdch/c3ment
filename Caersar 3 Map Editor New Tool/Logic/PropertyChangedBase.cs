﻿using System.ComponentModel;
using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class PropertyChangedBase : INotifyPropertyChanged
    {/*
        internal void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }*/
        public event PropertyChangedEventHandler PropertyChanged;
    }
}