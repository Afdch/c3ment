﻿using PropertyChanged;
using System.Collections.ObjectModel;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class TradePriceChange : PropertyChangedBase
    {

        public int ChangeYear { get; set; }
        public int ChangeMonth { get; set; }
        public int GoodId { get; set; }
        public int ChangeAmount { get; set; }
        public int ChangeId { get; set; }
        

        public static ObservableCollection<TradePriceChange> GetTradePriceChanges(BinaryReader br)
        {

            ObservableCollection<TradePriceChange> GetList = new ObservableCollection<TradePriceChange>();

            for (int i = 0; i < 20; i++)
            {
                int amount = HexReadWrite.ReadHexInt(br, 1204764 + i * 1, 1);
                GetList.Add
                (
                    new TradePriceChange()
                    {

                        ChangeYear = BinaryRW.ReadInt16(1204684 + i * 2),
                        ChangeMonth = BinaryRW.ReadByte(1204724 + i),
                        ChangeAmount = BinaryRW.ReadByte(1204784 + i),
                        GoodId = BinaryRW.ReadByte(1204744 + i),
                        ChangeId = i,
                    }
                );  
            }

            return GetList;
        }

        public static void SetTradePriceChanges(ObservableCollection<TradePriceChange> tradePriceChanges)
        {
            foreach (TradePriceChange tradePriceChange in tradePriceChanges)
            {
                
                BinaryRW.WriteInt16(1204684 + tradePriceChange.ChangeId * 2, tradePriceChange.ChangeYear); 
                BinaryRW.WriteByte(1204724 + tradePriceChange.ChangeId, (byte)tradePriceChange.ChangeMonth); 
                BinaryRW.WriteByte(1204744 + tradePriceChange.ChangeId, (byte)tradePriceChange.GoodId); 
                BinaryRW.WriteByte(1204784 + tradePriceChange.ChangeId, (byte)tradePriceChange.ChangeAmount); 
            }
        }


    }
}