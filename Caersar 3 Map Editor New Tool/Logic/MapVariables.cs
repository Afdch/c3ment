﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class MapVariables : PropertyChangedBase
    {/*
        #region win rating and loan values
        public int RequiredCulture;
        public int RequiredProsperity;
        public int RequiredPeace;
        public int RequiredFavor;

        public bool RequiredPopulationFlag;
        public int RequiredPopulation;

        public int ResqueLoan;
        #endregion

        #region earthquake
        public int EarthquakeSeverity;
        public int EarthquakeYear;
        public int EarthquakeXCoordinate;
        public int EarthquakeYCoordinate;
        #endregion


        #region flags
        public bool ChangeOfEmperorFlag;
        public int  ChangeOfEmperorYear;

        public bool GladiatorRevoltFlag; 
        int  gladiatorRevoltYear;

        public bool SeaTradeProblemsFlag;
        public bool LandTradeProblemsFlag;
        public bool RomeRaisesWagesFlag;
        public bool RomeLowersWagesFlag;
        bool contaminatedWaterFlag;
        bool ironMinesCollapseFlag;
        bool clayPitsFloodedFlag;

        #endregion

        #region dumbshit

        public bool IronMinesCollapseFlag
        {
            get => ironMinesCollapseFlag;
            set
            {
                if (ironMinesCollapseFlag != value)
                {
                    ironMinesCollapseFlag = value;
                    RaisePropertyChanged("IronMinesCollapseFlag");
                }
            }
        }
        public int GladiatorRevoltYear
        {
            get => gladiatorRevoltYear;
            set
            {
                if (gladiatorRevoltYear != value)
                {
                    gladiatorRevoltYear = value;
                    RaisePropertyChanged("IronMinesCollapseFlag");
                }
            }
        }
        public bool ClayPitsFloodedFlag
        {
            get => clayPitsFloodedFlag;
            set
            {
                if (clayPitsFloodedFlag != value)
                {
                    clayPitsFloodedFlag = value;
                    RaisePropertyChanged("ClayPitsFloodedFlag");
                }
            }
        }
        public bool ContaminatedWaterFlag
        {
            get => contaminatedWaterFlag;
            set
            {
                if (contaminatedWaterFlag != value)
                {
                    contaminatedWaterFlag = value;
                    RaisePropertyChanged("ContaminatedWaterFlag");
                }
            }
        }
        #endregion

        #region TODO

        /*
         * invasion: months
            invasion points x 
            invasion points y
            river entry point x
            river entry point y
            river exit point x
            river exit point y

            max years to achieve 25 / 50 / 75
             
             */

        /*#endregion

        /*

        public static void GetFlagsAndMapVariables (BinaryReader br, MapVariables mapVariables)
        {
            mapVariables.GladiatorRevoltFlag     = HexReadWrite.ReadHexInt(br, 1204804, 4) == 1 ? true : false;
            mapVariables.GladiatorRevoltYear     = HexReadWrite.ReadHexInt(br, 1204808, 4);
            mapVariables.ChangeOfEmperorFlag     = HexReadWrite.ReadHexInt(br, 1204812, 4) == 1 ? true : false;
            mapVariables.ChangeOfEmperorYear     = HexReadWrite.ReadHexInt(br, 1204816, 4);
            mapVariables.SeaTradeProblemsFlag    = HexReadWrite.ReadHexInt(br, 1204820, 4) == 1 ? true : false;
            mapVariables.LandTradeProblemsFlag   = HexReadWrite.ReadHexInt(br, 1204824, 4) == 1 ? true : false;
            mapVariables.RomeRaisesWagesFlag     = HexReadWrite.ReadHexInt(br, 1204828, 4) == 1 ? true : false;
            mapVariables.RomeLowersWagesFlag     = HexReadWrite.ReadHexInt(br, 1204832, 4) == 1 ? true : false;
            mapVariables.ContaminatedWaterFlag   = HexReadWrite.ReadHexInt(br, 1204836, 4) == 1 ? true : false;
            mapVariables.IronMinesCollapseFlag   = HexReadWrite.ReadHexInt(br, 1204840, 4) == 1 ? true : false;
            mapVariables.ClayPitsFloodedFlag     = HexReadWrite.ReadHexInt(br, 1204844, 4) == 1 ? true : false;
            
            mapVariables.RequiredCulture         = HexReadWrite.ReadHexInt(br, 1205104, 4);
            mapVariables.RequiredProsperity      = HexReadWrite.ReadHexInt(br, 1205108, 4);
            mapVariables.RequiredPeace           = HexReadWrite.ReadHexInt(br, 1205112, 4);
            mapVariables.RequiredFavor           = HexReadWrite.ReadHexInt(br, 1205116, 4);
            mapVariables.RequiredPopulationFlag  = HexReadWrite.ReadHexInt(br, 1205148, 4) == 1 ? true : false;
            mapVariables.RequiredPopulation      = HexReadWrite.ReadHexInt(br, 1205152, 4);
            mapVariables.ResqueLoan              = HexReadWrite.ReadHexInt(br, 1205208, 4);
            
            mapVariables.EarthquakeSeverity      = HexReadWrite.ReadHexInt(br, 1205140, 4); // 0 / 1 / 2 / 3
            mapVariables.EarthquakeYear          = HexReadWrite.ReadHexInt(br, 1205144, 4);
            mapVariables.EarthquakeXCoordinate   = HexReadWrite.ReadHexInt(br, 1205156, 2);
            mapVariables.EarthquakeYCoordinate = HexReadWrite.ReadHexInt(br, 1205158, 2);


        }
*/
    }
}
