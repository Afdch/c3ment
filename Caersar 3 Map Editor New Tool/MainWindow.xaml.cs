﻿using System;
using System.Windows;
using System.IO;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Caersar_3_Map_Editor_New_Tool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static List<Month> Months { get; set; } = Month.Months;

        #region map variables
        public int winningPopulation;

        public ObservableCollection<TradePriceChange> TradePriceChangesList { get; set; }
        public ObservableCollection<Building> AllowedBuildingsList { get; set; } = new ObservableCollection<Building>();
        /// <summary>
        /// all cities objects on the map
        /// </summary>
        public ObservableCollection<CityListItemViewModel> mapCities = new ObservableCollection<CityListItemViewModel>();
        /// <summary>
        /// trade cities on the map
        /// </summary>
        public ObservableCollection<CityListItemViewModel> tradeCities = new ObservableCollection<CityListItemViewModel>();
        //public static List<TradeGood> TradeGoodsList { get; set; } = TradeGood.DefaultTradeGoodsList;
        //public static MapVariables MapVariablesControl { get; set; } = new MapVariables();


        public ObservableCollection<TradeGood> TradeGoodsList { get; set; } = new ObservableCollection<TradeGood>();

        public static RequestsListViewModel RequestsListView { get; set; } = new RequestsListViewModel { RequestsList = new ObservableCollection<RequestsListViewModel.ImperialRequest>() };
        public static InvasionsViewModel InvasionsView { get; set; } = new InvasionsViewModel { Invasions = new ObservableCollection<InvasionsViewModel.Invasion>() };
        public static CityListViewModel CityListItems { get; set; } = new CityListViewModel { Items = new ObservableCollection<CityListItemViewModel>() };
        public static RatingsViewModel FlagsRatings { get; set; } = new RatingsViewModel { StartingFunds = 0 };
        public static ObservableCollection<QuotaChangesViewModel> QuotaChangesModel { get; set; } = new ObservableCollection<QuotaChangesViewModel>();
        public static QuotasListViewModel QuotasListViewModel { get; set; } = new QuotasListViewModel { Items = new ObservableCollection<QuotaChangesViewModel>()};

        private static string filename;

        #endregion

        #region test area


        #endregion

        #region map flags

        #endregion

        #region window variables
        OpenFileDialog openFileDialogue = new OpenFileDialog();
        SaveFileDialog saveFileDialogue = new SaveFileDialog();

        public static BinaryReader br;
        public static BinaryWriter bw;
        #endregion

        public MainWindow()
        {

            InitializeComponent();

        }

        #region UI elements clicks

    



        private void OpenSaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            openFileDialogue.Multiselect = false;
            openFileDialogue.Filter = "Caesar 3 uncompressed save file|*.c3u";
            openFileDialogue.DefaultExt = ".c3u";
            Nullable<bool> dialogueOK = openFileDialogue.ShowDialog();

            if (dialogueOK == true)
            {
                MainWindow.filename = openFileDialogue.FileName;
                br = new BinaryReader(File.OpenRead(MainWindow.filename));
                this.ReadSaveValues();
                br.Close();
                this.Title = $"Caesar 3 Map Editing tool 0.51 - {this.openFileDialogue.FileName.ToString()}";

            }
        }


        private void SaveSaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            /*
            saveFileDialogue.Filter = "Caesar 3 uncompressed save file|*.c3u";
            saveFileDialogue.DefaultExt = ".c3u";
            bool? dialogueOK = saveFileDialogue.ShowDialog();
            if (dialogueOK == true)
            {
                //string filename = saveFileDialogue.FileName;
            */
                if (MainWindow.filename == null) return;
                bw = new BinaryWriter(File.OpenWrite(MainWindow.filename));
                this.StoreValues();
                bw.Close();
            /*
            }
            */
        }

        private void StoreValues()
        {
            RatingsViewModel.SetMapVariables(FlagsRatings);
            TradeGood.SetPriceList(TradeGoodsList);
            TradePriceChange.SetTradePriceChanges(TradePriceChangesList);
            QuotaChangesViewModel.SetAllQuotaChangesList(QuotaChangesModel);
            Building.SetBuildingsAvailability();
            CityListItemViewModel.SetMapCityList(tradeCities);
        }

        #endregion
        /// <summary>
        /// Reads the save file and fills in all the nessessary lists to work with
        /// </summary>
        private void ReadSaveValues()
        {

            #region trade goods prices and imperial 
            
            //get imperial requests
            if (RequestsListView.RequestsList.Count > 0) RequestsListView.RequestsList.Clear();
            foreach (RequestsListViewModel.ImperialRequest request in RequestsListViewModel.GetRequestedGoods(br)) RequestsListView.RequestsList.Add(request);

            //get invasions
            if (InvasionsView.Invasions.Count > 0) InvasionsView.Invasions.Clear();
            foreach (InvasionsViewModel.Invasion invasion in InvasionsViewModel.GetInvasionData(br)) InvasionsView.Invasions.Add(invasion);




            //get map and trade cities
            if (mapCities.Count > 0) mapCities.Clear();
            foreach (CityListItemViewModel cityListItem in CityListItemViewModel.GetMapCityList()) mapCities.Add(cityListItem);
            // get trade cities on the map
            this.tradeCities = CityListItemViewModel.GetTradeCityList(mapCities);




            // create viewmodels for each trade city
            if (CityListItems.Items.Count > 0) CityListItems.Items.Clear();
            foreach (CityListItemViewModel city in tradeCities) CityListItems.Items.Add(city);





            //get all quotas lists
            CityListItemViewModel.QuotasList = CityListItemViewModel.GetQuotasList();
            if (QuotaChangesModel.Count > 0) QuotaChangesModel.Clear();
            foreach (QuotaChangesViewModel quota in QuotaChangesViewModel.AllQuotaChangesList())
            {
                QuotaChangesModel.Add(quota);
                QuotasListViewModel.Items.Add(quota);
            }

            foreach(CityListItemViewModel city in CityListItems.Items)
            {
                if (city.QuotasListViewModel == null) city.QuotasListViewModel = new QuotasListViewModel { Items = new ObservableCollection<QuotaChangesViewModel>() };
                if (city.QuotasListViewModel.Items.Count > 0) city.QuotasListViewModel.Items.Clear();
                foreach (QuotaChangesViewModel quota in QuotaChangesModel.Where(x => x.TradeRouteID == city.quotaOffset))
                {
                    city.QuotasListViewModel.Items.Add(quota);
                }

            }

            /*this.quotaChanges.Text = "";
            for (int i = 0; i < 20; i++)
            {
                int YearOffset =                                          HexReadWrite.ReadHexInt(MainWindow.br, 1204564 + i * 2, 2);
                int Month =                                               HexReadWrite.ReadHexInt(MainWindow.br, 1204604 + i, 1);
                TradeGood TradeGoodItem = TradeGood.DefaultTradeGoodsList[HexReadWrite.ReadHexInt(MainWindow.br, 1204624 + i, 1)];
                int TradeRouteID =                                        HexReadWrite.ReadHexInt(MainWindow.br, 1204644 + i, 1);
                bool IsQuotaIncrease =                                    HexReadWrite.ReadHexInt(MainWindow.br, 1204664 + i, 1) == 1 ? true : false;
                this.quotaChanges.Text += $"In {YearOffset}, {Months[Month].MonthName}, trade quota for {TradeGoodItem.GoodName} {(IsQuotaIncrease ? "increases" : "decreases")} for city №{TradeRouteID}\n";
            }*/


            //TODO define quotas lists for each city

            #region allowed buildings and goods
            // Get awailable buildings

            Building.GetBuildingsAvailability();
            if (AllowedBuildingsList.Count > 0) AllowedBuildingsList.Clear();
            foreach (Building building in Building.allowedBuildingsList) this.AllowedBuildingsList.Add(building);

            //TODO make 3 columns instead of a single one

            #endregion







            // get buying and selling p    
            List<int> buyPricesList = TradeGood.GetPriceList(br, "buyPrices");
            List<int> sellPricesList = TradeGood.GetPriceList(br, "sellPrices");
            if (TradeGoodsList != null)
            {
                TradeGoodsList.Clear();
            } else TradeGoodsList = new ObservableCollection<TradeGood>();
            TradeGood.GetPriceList();
            foreach (TradeGood tradeGood in TradeGood.DefaultTradeGoodsList)
            {
                TradeGoodsList.Add(
                    new TradeGood
                    {
                        GoodID = tradeGood.GoodID,
                        BuyersPay = tradeGood.BuyersPay,
                        SellersGet = tradeGood.SellersGet,
                        GoodImage = tradeGood.GoodImage,
                        GoodName = tradeGood.GoodName,

                    }
                    );

                //TradeGoodsList[tradeGood.GoodID].BuyersPay = buyPricesList[tradeGood.GoodID];
                //TradeGoodsList[tradeGood.GoodID].SellersGet = sellPricesList[tradeGood.GoodID];
            }




            if (this.TradePriceChangesList != null) displayMapPropertiesTextBlock.Text += $"\n{this.TradePriceChangesList.Count}";

            // get and set trade price changes
            // todo: redo this
            if (TradePriceChangesList != null)
            {
                TradePriceChangesList.Clear();
                ObservableCollection<TradePriceChange> tradePriceChanges = new ObservableCollection<TradePriceChange>();
                tradePriceChanges = TradePriceChange.GetTradePriceChanges(br);
                foreach (TradePriceChange tradePriceChange in tradePriceChanges) TradePriceChangesList.Add(tradePriceChange);
            }else TradePriceChangesList = TradePriceChange.GetTradePriceChanges(br);




            #endregion

            #region flags and variables
            //TODO check flags allocation, feels not quite right
            RatingsViewModel.GetFlagsAndMapVariables(FlagsRatings);

            //Get City Name
            FlagsRatings.CityName = mapCities.First(x => x.cityTypeID == 1).CityName;




            // todo delet this

            displayMapPropertiesTextBlock.Text += $"\nrequired {FlagsRatings.RequiredPopulation} pop";
            displayMapPropertiesTextBlock.Text += $"\nStarting date is {HexReadWrite.ReadHexInt16(1203532)} ";
            foreach (CityListItemViewModel tradeCity in tradeCities)
                displayMapPropertiesTextBlock.Text += $"\nTrade with {CityListItemViewModel.cityNames[tradeCity.cityNameId]}";


            #endregion



            this.DataContext = this;


        }



    }
}
