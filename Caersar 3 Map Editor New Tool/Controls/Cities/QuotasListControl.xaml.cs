﻿using System.Windows.Controls;

namespace Caersar_3_Map_Editor_New_Tool
{
    /// <summary>
    /// Interaction logic for QuotasListControl.xaml
    /// </summary>
    public partial class QuotasListControl : UserControl
    {
        public QuotasListControl()
        {
            InitializeComponent();
        }

        private void AddQuotaButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            return;
            if (MainWindow.QuotaChangesModel.Count == 20)
            {
                return;
            }
            QuotaChangesViewModel quota = new QuotaChangesViewModel();

            //TODO:
            //Open a new window with new quota's parameters to set
            //On clicking OK store these paremeters into the quota
            
            MainWindow.QuotaChangesModel.Add(quota);
        }
    }
}
