﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class CityListViewModel : BaseViewModel
    {
        public ObservableCollection<CityListItemViewModel> Items { get; set; }

    }
}
