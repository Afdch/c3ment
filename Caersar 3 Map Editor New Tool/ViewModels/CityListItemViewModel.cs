﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class CityListItemViewModel : BaseViewModel
    {
        #region static lists
        public static readonly List<string> cityNames = new List<string>()
        {
            "Roma",
            "Tarentum",
            "Capua",
            "Brundisium",
            "Mediolanum",
            "Carthago Nova",
            "Carthago",
            "Tarraco",
            "Athenae",
            "Pergamum",
            "Syracuse",
            "Toletum",
            "Tarsus",
            "Leptis Magna",
            "Tingis",
            "Corinthus",
            "Valentia",
            "Ephesus",
            "Miletus",
            "Sinope",
            "Cyrene",
            "Antioch",
            "Heliopolis",
            "Damascus",
            "Hierosolyma",
            "Lindum",
            "Calleva",
            "Lutetia",
            "Massilia",
            "Narbo",
            "Lugdunum",
            "Caesarea",
            "Alexandria",
            "Augusta Trevorum",
            "Argentoratum",
            "Volubilis",
            "Londinium",
            "Thamugadi",
            "Sarmizegetusa",
            "Byzantium",

        };
        public static readonly List<string> cityTypes = new List<string>()
        {
            "A Roman City",
            "Our city!",
            "Trade Route",
            "Trade Route",
            "A distant city",
            "A Roman City"
        };
        public static List<List<int>> QuotasList;
        #endregion


        #region properties

        public string CityName { get; set; }
        public string OpenTradeCost { get; set; }
        public bool IsSeaTradeRoute { get; set; }

        public List<int> CityQuotas { get; set; }
        //public ObservableCollection<QuotaChangesViewModel> CityQuotasChanges { get; set; }
        public QuotasListViewModel QuotasListViewModel { get; set; }

        public Good Wheat       { get; set; }
        public Good Vegetables  { get; set; }
        public Good Fruits      { get; set; }
        public Good Olives      { get; set; }
        public Good Vines       { get; set; }
        public Good Meat        { get; set; }
        public Good Wine        { get; set; }
        public Good Oil         { get; set; }
        public Good Iron        { get; set; }
        public Good Timber      { get; set; }
        public Good Clay        { get; set; }
        public Good Marble      { get; set; }
        public Good Weapons     { get; set; }
        public Good Furniture   { get; set; }
        public Good Pottery     { get; set; }


        public class Good
        {
            public int GoodQuota { get; set; }
            public bool? IsCityBuyingGood { get; set; }
            public TradeGood CityGood { get; set; }
        }

        #endregion

        #region fields

        public bool isCityActive;
        public int cityTypeID;
        public int cityNameId = 0;
        public int quotaOffset;
        public bool isTradeOpened = false;
        public List<bool> buysFlagsList = new List<bool>(16);
        public List<bool> sellsFlagsList = new List<bool>(16);
        public int openTradeCost;
        public int empireObjID;
        public bool isSeaTradeRoute;

        #endregion

        #region methods
        /// <summary>
        /// Gets a quota list
        /// </summary>
        public static List<List<int>> GetQuotasList()
        {
            int quotaLen = 10;
            List<List<int>> quota = new List<List<int>>();
            for (int i = 0; i < quotaLen; i++)
            {
                quota.Add(GetSingleQuota(i));
            }
            return quota;
        }

        static List<int> GetSingleQuota(int quotaNumber)
        {
            int offset = 1251074;
            List<int> singleQuota = new List<int>();

            for ( int j = 0; j < 16; j ++)
            {
                singleQuota.Add( HexReadWrite.ReadHexInt(MainWindow.br, offset + 64 * quotaNumber + j * 4, 4));
            }
            return singleQuota;
        }


        private static void PrepareCities(ObservableCollection<CityListItemViewModel> cities)
        {
            foreach (CityListItemViewModel city in cities)
            {

                city.CityName = CityListItemViewModel.cityNames[city.cityNameId];
                city.OpenTradeCost = city.openTradeCost.ToString();
                city.IsSeaTradeRoute = city.isSeaTradeRoute;

                city.CityQuotas = GetSingleQuota(city.quotaOffset);
                //city.QuotasListViewModel.Items = QuotaChangesViewModel.QuotaChangesForCity(city.quotaOffset);

                city.Wheat      = GetGood(city, TradeGood.DefaultTradeGoodsList[1]);
                city.Vegetables = GetGood(city, TradeGood.DefaultTradeGoodsList[2]);
                city.Fruits     = GetGood(city, TradeGood.DefaultTradeGoodsList[3]);
                city.Olives     = GetGood(city, TradeGood.DefaultTradeGoodsList[4]);
                city.Vines      = GetGood(city, TradeGood.DefaultTradeGoodsList[5]);
                city.Meat       = GetGood(city, TradeGood.DefaultTradeGoodsList[6]);
                city.Wine       = GetGood(city, TradeGood.DefaultTradeGoodsList[7]);
                city.Oil        = GetGood(city, TradeGood.DefaultTradeGoodsList[8]);
                city.Iron       = GetGood(city, TradeGood.DefaultTradeGoodsList[9]);
                city.Timber     = GetGood(city, TradeGood.DefaultTradeGoodsList[10]);
                city.Clay       = GetGood(city, TradeGood.DefaultTradeGoodsList[11]);
                city.Marble     = GetGood(city, TradeGood.DefaultTradeGoodsList[12]);
                city.Weapons    = GetGood(city, TradeGood.DefaultTradeGoodsList[13]);
                city.Furniture  = GetGood(city, TradeGood.DefaultTradeGoodsList[14]);
                city.Pottery    = GetGood(city, TradeGood.DefaultTradeGoodsList[15]);

                

            }
            
        }

        private static void PrepareBuySellListForSaving(ObservableCollection<CityListItemViewModel> cities)
        {
            foreach(CityListItemViewModel city in cities)
            {
                city.buysFlagsList[1] =  city.Wheat      .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[2] =  city.Vegetables .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[3] =  city.Fruits     .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[4] =  city.Olives     .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[5] =  city.Vines      .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[6] =  city.Meat       .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[7] =  city.Wine       .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[8] =  city.Oil        .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[9] =  city.Iron       .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[10] = city.Timber     .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[11] = city.Clay       .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[12] = city.Marble     .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[13] = city.Weapons    .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[14] = city.Furniture  .IsCityBuyingGood == true ? true : false;
                city.buysFlagsList[15] = city.Pottery    .IsCityBuyingGood == true ? true : false;

                city.sellsFlagsList[1] =  city.Wheat      .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[2] =  city.Vegetables .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[3] =  city.Fruits     .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[4] =  city.Olives     .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[5] =  city.Vines      .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[6] =  city.Meat       .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[7] =  city.Wine       .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[8] =  city.Oil        .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[9] =  city.Iron       .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[10] = city.Timber     .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[11] = city.Clay       .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[12] = city.Marble     .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[13] = city.Weapons    .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[14] = city.Furniture  .IsCityBuyingGood == false ? true : false;
                city.sellsFlagsList[15] = city.Pottery    .IsCityBuyingGood == false ? true : false;

                QuotasList[city.quotaOffset] = new List<int>
                {
                    0,
                    city.Wheat.GoodQuota,
                    city.Vegetables .GoodQuota,
                    city.Fruits.GoodQuota,

                    city.Olives.GoodQuota,
                    city.Vines.GoodQuota,
                    city.Meat.GoodQuota,
                    city.Wine.GoodQuota,

                    city.Oil.GoodQuota,
                    city.Iron.GoodQuota,
                    city.Timber.GoodQuota,
                    city.Clay.GoodQuota,

                    city.Marble.GoodQuota,
                    city.Weapons.GoodQuota,
                    city.Furniture.GoodQuota,
                    city.Pottery.GoodQuota
                };
            }
        }

        /// <summary>
        /// Returns the <see cref="Good"/> object for a <see cref="CityListItemViewModel"/> city
        /// </summary>
        /// <param name="city">The city the trade good is returned for</param>
        /// <param name="cityGood">The trade good that is returned</param>
        /// <returns></returns>
        public static Good GetGood(CityListItemViewModel city, TradeGood cityGood)
        {
            Good good = new Good();
            good.CityGood = cityGood;
            good.IsCityBuyingGood = !(city.buysFlagsList[cityGood.GoodID] || city.sellsFlagsList[cityGood.GoodID]) ? null : (bool?)city.buysFlagsList[cityGood.GoodID];
            good.GoodQuota = city.cityTypeID == 2 ? city.CityQuotas[cityGood.GoodID] : 0;

            return good;

        }

        public static ObservableCollection<CityListItemViewModel> GetTradeCityList()
            => new ObservableCollection<CityListItemViewModel>(GetMapCityList().Where(x => x.cityTypeID == 2).ToList());

        public static ObservableCollection<CityListItemViewModel> GetTradeCityList(ObservableCollection<CityListItemViewModel> cityList)
            => new ObservableCollection<CityListItemViewModel>(cityList.Where(x => x.cityTypeID == 2).ToList());

        /// <summary>
        /// Saves all city values and the traded goods for each city
        /// </summary>
        /// <param name="cityList"></param>
        public static void SetMapCityList(ObservableCollection<CityListItemViewModel> cityList)
        {
            PrepareBuySellListForSaving(cityList);
            foreach (CityListItemViewModel city in cityList)
            {
                int offset = 1200492 + city.empireObjID * 66;
                BinaryRW.WriteBool(offset, city.isCityActive);
                BinaryRW.WriteByte(offset + 2, (byte)city.cityTypeID);
                BinaryRW.WriteByte(offset + 3, (byte)city.cityNameId);
                BinaryRW.WriteByte(offset + 4, (byte)city.quotaOffset);
                BinaryRW.WriteBool(offset + 5, city.isTradeOpened);
                for (int i = 0; i < 16; i++)
                {
                    BinaryRW.WriteBool(offset + 6 + i, city.buysFlagsList[i]);
                }
                for (int i = 0; i < 16; i++)
                {
                    BinaryRW.WriteBool(offset + 22 + i, city.sellsFlagsList[i]);
                }
                BinaryRW.WriteInt16(offset + 38, System.Convert.ToInt16(city.OpenTradeCost)); //TODO check if works
                BinaryRW.WriteBool(offset + 48, city.IsSeaTradeRoute); //TODO check if works
            }

            SetTradeQuotasList();
        }
        
        private static void SetTradeQuotasList()
        {
            for (int listNumber = 0; listNumber < QuotasList.Count; listNumber++)
            {

                List<int> currentQuotaList = QuotasList[listNumber];
                
                int offset = 1251074 + listNumber * 64;
                for (int quotaNumber = 0; quotaNumber < 16;  quotaNumber++)
                {
                    int singleOffset = offset + quotaNumber * 4;
                    BinaryRW.WriteInt32(singleOffset, currentQuotaList[quotaNumber]);

                }
            }

            /*
            foreach (List<int> quotas in QuotasList)
            {
                int a = QuotasList.IndexOf(quotas);
                int offset = 1251074 + QuotasList.IndexOf(quotas) * 64;
                foreach (int goodQuota in quotas)
                {
                    int b = quotas.IndexOf(goodQuota);
                    int singleOffset = offset + quotas.IndexOf(goodQuota);
                    BinaryRW.WriteInt32(singleOffset, goodQuota);
                }
            }*/
        }


        public static ObservableCollection<CityListItemViewModel> GetMapCityList()
        {
            BinaryReader br = MainWindow.br;
            ObservableCollection<CityListItemViewModel> mapCitiesList = new ObservableCollection<CityListItemViewModel>();
            int citiesTotal = 40;
            for (int j = 0; j < citiesTotal; j++)
            {
                int offset = 1200492 + j * 66;

                List<bool> buysList = new List<bool>(16);
                List<bool> sellsList = new List<bool>(16);
                for (int i = 0; i < 16; i++)
                {
                    
                    buysList.Add(HexReadWrite.ReadHexInt(br, offset + 6 + i, 1) != 0 ? true : false);
                }
                for (int i = 0; i < 16; i++)
                {
                    sellsList.Add(HexReadWrite.ReadHexInt(br, offset + 22 + i, 1) != 0 ? true : false);
                }
                mapCitiesList.Add(new CityListItemViewModel
                {
                    isCityActive = HexReadWrite.ReadHexInt(br, offset, 1) != 0 ? true : false,
                    cityTypeID = HexReadWrite.ReadHexInt(br, offset + 2, 1),
                    cityNameId = HexReadWrite.ReadHexInt(br, offset + 3, 1),
                    quotaOffset = HexReadWrite.ReadHexInt(br, offset + 4, 1),
                    isTradeOpened = HexReadWrite.ReadHexInt(br, offset + 5, 1) == 1 ? true : false,
                    buysFlagsList = buysList,
                    sellsFlagsList = sellsList,
                    openTradeCost = HexReadWrite.ReadHexInt(br, offset + 38, 2),
                    empireObjID = HexReadWrite.ReadHexInt(br, offset + 46, 1),
                    isSeaTradeRoute = HexReadWrite.ReadHexInt(br, offset + 48, 1) == 1 ? true : false,

                });
                PrepareCities(mapCitiesList);
            }
            return new ObservableCollection<CityListItemViewModel>(mapCitiesList.OrderBy(o => o.empireObjID).ToList());
        }
        #endregion

        #region temporary
        public static string GetTradeCity(BinaryReader br, int offset)
        {
            string tradeCity = "";
            for (int i = offset; i < offset + 66; i++)
            {
                tradeCity += $"{HexReadWrite.ReadHexInt(br, i, 1)}\t";
            }
            return $"{tradeCity}\n";
        }
        public static string GetTradeQuota(BinaryReader br, int offset)
        {
            string tradeQuota = "";
            for (int i = 0; i < 16; i++)
            {
                tradeQuota += $"{HexReadWrite.ReadHexInt(br, offset + i * 4, 4)}\t";
            }
            return $"{tradeQuota}\n";
        }
        #endregion

        public static CityListItemViewModel GetCity(BinaryReader br)
        {
            return null;
        }
    }
}
