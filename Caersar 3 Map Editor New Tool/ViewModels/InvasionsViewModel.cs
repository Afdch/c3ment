﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]
    public class InvasionsViewModel : BaseViewModel
    {
        /*
        public List<int> InvasionYears          = new List<int>(20);
        public List<int> InvasionBattleTypes    = new List<int>(20); //  0: no invasion, 1: local uprising, 2: enemy army, 4: distant battle
        public List<int> InvasionArmySizes      = new List<int>(20);
        public List<int> InvasionPointIndexes   = new List<int>(20); // from 0 to 7; default is 8, i.e. not applicable
        public List<int> InvasionPointsX        = new List<int>(20);
        public List<int> InvasionPointsY        = new List<int>(20);
        public int InvasionEnemyType;
        */
        public ObservableCollection<Invasion> Invasions { get; set; }

        public static InvasionsViewModel Instance => new InvasionsViewModel();

        public InvasionsViewModel()
        {
            Invasions = new ObservableCollection<Invasion>

            {
                new Invasion
                {
                    InvYear = 10,
                    InvType = 1,
                    InvSize = 2,
                    InvIndex = 5,

                },
                new Invasion
                {
                    InvYear = 15,
                    InvType = 2,
                    InvSize = 4,
                    InvIndex = 7,

                },
            };
        }

        public class Invasion : BaseViewModel
        {
            public int InvYear { get; set; }
            public int InvType { get; set; }
            public int InvSize { get; set; }
            public int InvIndex { get; set; }
        }

        public static ObservableCollection<Invasion> GetInvasionData(BinaryReader br)
        {
            int invasionsOffset = 1203706;
            ObservableCollection<Invasion> invasions = new ObservableCollection<Invasion>();


            for (int i = 0; i < 20; i++)
            {
                invasions.Add(new Invasion()
                {
                    InvYear = HexReadWrite.ReadHexInt(br, invasionsOffset + 2 * i, 2),
                    InvType = HexReadWrite.ReadHexInt(br, invasionsOffset + 40 + 2 * i, 2),
                    InvSize = HexReadWrite.ReadHexInt(br, invasionsOffset + 80 + 2 * i, 2),
                    InvIndex = HexReadWrite.ReadHexInt(br, invasionsOffset + 120 + 2 * i, 2),
                });
            }
            return invasions;
        }


        public static void SetInvasionData(BinaryWriter bw, ObservableCollection<Invasion> invasions)
        {
            int invasionsOffset = 1203706;
            for (int i = 0; i < 20; i++)
            {
                HexReadWrite.WriteHexInt(bw, invasionsOffset + i * 2, 2, invasions[i].InvYear);
                HexReadWrite.WriteHexInt(bw, invasionsOffset + 40 + i * 2, 2, invasions[i].InvType);
                HexReadWrite.WriteHexInt(bw, invasionsOffset + 80 + i * 2, 2, invasions[i].InvSize);
                HexReadWrite.WriteHexInt(bw, invasionsOffset + 120 + i * 2, 2, invasions[i].InvIndex);
            }
        }

    }
}
