﻿using System.Collections.ObjectModel;
using System.Linq;
using PropertyChanged;



namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]
    public class QuotaChangesViewModel : BaseViewModel
    {
        public static ObservableCollection<QuotaChangesViewModel> QuotaChangesViewModelList = new ObservableCollection<QuotaChangesViewModel>();

        /// <summary>
        /// the year the change happens, adds to the starting date
        /// </summary>
        public int YearOffset { get; set; }

        /// <summary>
        /// month of the year the change happens
        /// 0-11
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// The trade good the quota changes for
        /// (0) 1-15
        /// </summary>
        public TradeGood TradeGoodItem { get; set; }

        /// <summary>
        /// Quota ID
        /// </summary>
        public int TradeRouteID { get; set; }

        /// <summary>
        /// if <see langword="true"/>, the quota increases, if <see langword="false"/>, the quota decreses
        /// 0-15-25-40
        /// </summary>
        public bool IsQuotaIncrease { get; set; }


        #region methods

        /// <summary>
        /// Defines a collection of quota changes for a specified trade route (trade quota ID) 
        /// </summary>
        /// <param name="quotaId">The quota ID the quota changes is returned for</param>
        /// <returns>a <see cref="ObservableCollection{QuotaChangesViewModel}"/> for a <paramref name="quotaId"/></returns>
        public static ObservableCollection<QuotaChangesViewModel> QuotaChangesForCity(int quotaId) => new ObservableCollection<QuotaChangesViewModel>(AllQuotaChangesList().Where(quotaChange => quotaChange.TradeRouteID == quotaId).ToList());

        /// <summary>
        /// Gets a full list of quota changes for all trade routes
        /// </summary>
        /// <returns>a <see cref="ObservableCollection{QuotaChangesViewModel}"/> for all cities</returns>
        public static ObservableCollection<QuotaChangesViewModel> AllQuotaChangesList()
        {
            ObservableCollection<QuotaChangesViewModel> quotaChanges = new ObservableCollection<QuotaChangesViewModel>();

            for (int i = 0; i < 20; i++)
            {
                quotaChanges.Add(new QuotaChangesViewModel
                {
                    YearOffset = HexReadWrite.ReadHexInt(MainWindow.br, 1204564 + i * 2, 2),
                    Month = HexReadWrite.ReadHexInt(MainWindow.br, 1204604 + i, 1),
                    TradeGoodItem = TradeGood.DefaultTradeGoodsList[HexReadWrite.ReadHexInt(MainWindow.br, 1204624 + i, 1)],
                    TradeRouteID = HexReadWrite.ReadHexInt(MainWindow.br, 1204644 + i, 1),
                    IsQuotaIncrease = HexReadWrite.ReadHexInt(MainWindow.br, 1204784 + i, 1) == 1 ? true : false

                });
            }

            return quotaChanges;
        }

        /// <summary>
        /// Stores a full list of quota changes for all trade routes in a save file
        /// </summary>
        /// <returns>a <see cref="ObservableCollection{QuotaChangesViewModel}"/> for all cities</returns>
        public static void SetAllQuotaChangesList(ObservableCollection<QuotaChangesViewModel> quotaChanges)
        {

            for (int i = 0; i < 20; i++)
            {
                BinaryRW.WriteInt16(1204564 + i * 2, quotaChanges[i].YearOffset);
                BinaryRW.WriteByte(1204604 + i, (byte)quotaChanges[i].Month);
                BinaryRW.WriteByte(1204624 + i, (byte)quotaChanges[i].TradeGoodItem.GoodID);
                BinaryRW.WriteByte(1204644 + i, (byte)quotaChanges[i].TradeRouteID);
                BinaryRW.WriteBool(1204784 + i, quotaChanges[i].IsQuotaIncrease);
            }
            
        }

        public static QuotasListViewModel AllQuotaChanges()
        {
            return new QuotasListViewModel { Items = AllQuotaChangesList() };
        }

        #endregion
    }
}
