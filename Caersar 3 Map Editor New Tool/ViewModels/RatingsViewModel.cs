﻿using PropertyChanged;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]
    public class RatingsViewModel : BaseViewModel
    {


        #region starting conditions
        public int StartingFunds { get; set; }
        public int StartingDate { get; set; }
        public string CityName { get; set; }
        public string GovernorName { get; set; } = "";
        //todo initial rank

        //todo 25/50/75% milestones
        //todo rome supplies wheat?
        //todo flotsam on?

        #endregion

        #region win rating and loan values
        public int RequiredCulture { get; set; } = 0;
        public int RequiredProsperity { get; set; } = 0;
        public int RequiredPeace { get; set; } = 0;
        public int RequiredFavor { get; set; } = 0;

        public bool RequiredPopulationFlag { get; set; }
        public int RequiredPopulation { get; set; }

        public int ResqueLoan { get; set; }
        #endregion

        #region earthquake
        public int EarthquakeSeverity { get; set; }
        public int EarthquakeYear { get; set; }
        public int EarthquakeYear2 { get; set; } //TODO
        public int EarthquakeXCoordinate { get; set; }
        public int EarthquakeYCoordinate { get; set; }
        public int EarthquakeX1Coordinate { get; set; }
        public int EarthquakeY1Coordinate { get; set; }
        public int EarthquakeX2Coordinate { get; set; }
        public int EarthquakeY2Coordinate { get; set; }
        public int EarthquakeX3Coordinate { get; set; }
        public int EarthquakeY3Coordinate { get; set; }
        public int EarthquakeX4Coordinate { get; set; }
        public int EarthquakeY4Coordinate { get; set; }
        #endregion

        #region flags
        public bool ChangeOfEmperorFlag { get; set; }
        public int ChangeOfEmperorYear { get; set; }

        public bool GladiatorRevoltFlag { get; set; }
        public int GladiatorRevoltYear { get; set; }

        public bool SeaTradeProblemsFlag { get; set; }      = false;
        public bool LandTradeProblemsFlag { get; set; }     = false;
        public bool RomeRaisesWagesFlag { get; set; }       = false;
        public bool RomeLowersWagesFlag { get; set; }       = false;
        public bool ContaminatedWaterFlag { get; set; } = false;
        public bool IronMinesCollapseFlag { get; set; } = false;
        public bool ClayPitsFloodedFlag { get; set; } = false;

        #endregion


        public static void GetFlagsAndMapVariables(RatingsViewModel mapVariables)
        {

            BinaryReader br = MainWindow.br;

            mapVariables.GovernorName = HexReadWrite.ReadGovernorName();

            mapVariables.StartingFunds = HexReadWrite.ReadHexInt(br, 1203908, 4);
            mapVariables.StartingDate = HexReadWrite.ReadHexInt16(1203532);

            mapVariables.GladiatorRevoltFlag = HexReadWrite.ReadHexInt(br, 1204804, 4) == 1 ? true : false;
            mapVariables.GladiatorRevoltYear = HexReadWrite.ReadHexInt(br, 1204808, 4);
            mapVariables.ChangeOfEmperorFlag = HexReadWrite.ReadHexInt(br, 1204812, 4) == 1 ? true : false;
            mapVariables.ChangeOfEmperorYear = HexReadWrite.ReadHexInt(br, 1204816, 4);

            mapVariables.SeaTradeProblemsFlag = HexReadWrite.ReadHexInt(br, 1204820, 4) == 1 ? true : false;
            mapVariables.LandTradeProblemsFlag = HexReadWrite.ReadHexInt(br, 1204824, 4) == 1 ? true : false;
            mapVariables.RomeRaisesWagesFlag = HexReadWrite.ReadHexInt(br, 1204828, 4) == 1 ? true : false;
            mapVariables.RomeLowersWagesFlag = HexReadWrite.ReadHexInt(br, 1204832, 4) == 1 ? true : false;
            mapVariables.ContaminatedWaterFlag = HexReadWrite.ReadHexInt(br, 1204836, 4) == 1 ? true : false;
            mapVariables.IronMinesCollapseFlag = HexReadWrite.ReadHexInt(br, 1204840, 4) == 1 ? true : false;
            mapVariables.ClayPitsFloodedFlag = HexReadWrite.ReadHexInt(br, 1204844, 4) == 1 ? true : false;

            mapVariables.RequiredCulture = HexReadWrite.ReadHexInt(br, 1205104, 4);
            mapVariables.RequiredProsperity = HexReadWrite.ReadHexInt(br, 1205108, 4);
            mapVariables.RequiredPeace = HexReadWrite.ReadHexInt(br, 1205112, 4);
            mapVariables.RequiredFavor = HexReadWrite.ReadHexInt(br, 1205116, 4);
            mapVariables.RequiredPopulationFlag = HexReadWrite.ReadHexInt(br, 1205148, 4) == 1 ? true : false;
            mapVariables.RequiredPopulation = HexReadWrite.ReadHexInt(br, 1205152, 4);
            mapVariables.ResqueLoan = HexReadWrite.ReadHexInt(br, 1205208, 4);

            mapVariables.EarthquakeSeverity = HexReadWrite.ReadHexInt(br, 1205140, 4); // 0 / 1 / 2 / 3
            mapVariables.EarthquakeYear = HexReadWrite.ReadHexInt(br, 1205144, 4);
            mapVariables.EarthquakeYear2 = HexReadWrite.ReadHexInt(br, 1205256, 4) - mapVariables.EarthquakeYear;
            mapVariables.EarthquakeXCoordinate = HexReadWrite.ReadHexInt16(1205156);
            mapVariables.EarthquakeYCoordinate = HexReadWrite.ReadHexInt16(1205158);

            mapVariables.EarthquakeX1Coordinate = HexReadWrite.ReadHexInt16(1205284);
            mapVariables.EarthquakeY1Coordinate = HexReadWrite.ReadHexInt16(1205292);
            mapVariables.EarthquakeX2Coordinate = HexReadWrite.ReadHexInt16(1205300);
            mapVariables.EarthquakeY2Coordinate = HexReadWrite.ReadHexInt16(1205308);
            mapVariables.EarthquakeX3Coordinate = HexReadWrite.ReadHexInt16(1205288);
            mapVariables.EarthquakeY3Coordinate = HexReadWrite.ReadHexInt16(1205296);
            mapVariables.EarthquakeX4Coordinate = HexReadWrite.ReadHexInt16(1205304);
            mapVariables.EarthquakeY4Coordinate = HexReadWrite.ReadHexInt16(1205312);
            

        }

        public static void SetMapVariables(RatingsViewModel mapVariables)
        {

            //TODO: write governor name

            BinaryRW.WriteInt32(1203908, mapVariables.StartingFunds);
            BinaryRW.WriteInt16(1203532, mapVariables.StartingDate);

            BinaryRW.WriteBool(1204804, mapVariables.GladiatorRevoltFlag);
            BinaryRW.WriteInt32(1204808, mapVariables.GladiatorRevoltYear);
            BinaryRW.WriteBool(1204812, mapVariables.ChangeOfEmperorFlag);
            BinaryRW.WriteInt32(1204816, mapVariables.ChangeOfEmperorYear);

            BinaryRW.WriteBool(1204820, mapVariables.SeaTradeProblemsFlag);
            BinaryRW.WriteBool(1204824, mapVariables.LandTradeProblemsFlag);
            BinaryRW.WriteBool(1204828, mapVariables.RomeRaisesWagesFlag);
            BinaryRW.WriteBool(1204832, mapVariables.RomeLowersWagesFlag);
            BinaryRW.WriteBool(1204836, mapVariables.ContaminatedWaterFlag);
            BinaryRW.WriteBool(1204840, mapVariables.IronMinesCollapseFlag);
            BinaryRW.WriteBool(1204844, mapVariables.ClayPitsFloodedFlag);

            BinaryRW.WriteInt32(1205104, mapVariables.RequiredCulture);
            BinaryRW.WriteInt32(1205108, mapVariables.RequiredProsperity);
            BinaryRW.WriteInt32(1205112, mapVariables.RequiredPeace);
            BinaryRW.WriteInt32(1205116, mapVariables.RequiredFavor);
            BinaryRW.WriteBool (1205148, mapVariables.RequiredPopulationFlag);
            BinaryRW.WriteInt32(1205152, mapVariables.RequiredPopulation);
            BinaryRW.WriteInt32(1205208, mapVariables.ResqueLoan);

            //todo check all earthquake allocations for severity etc.

            BinaryRW.WriteInt32(1205140, mapVariables.EarthquakeSeverity);
            BinaryRW.WriteInt32(1205144, mapVariables.EarthquakeYear);
            BinaryRW.WriteInt32(1205256, mapVariables.EarthquakeYear2 + mapVariables.EarthquakeYear);
            BinaryRW.WriteInt16(1205156, mapVariables.EarthquakeXCoordinate);
            BinaryRW.WriteInt16(1205158, mapVariables.EarthquakeYCoordinate);
            

            BinaryRW.WriteInt16(1205284, mapVariables.EarthquakeXCoordinate);
            BinaryRW.WriteInt16(1205292, mapVariables.EarthquakeXCoordinate);

            BinaryRW.WriteInt16(1205300, mapVariables.EarthquakeXCoordinate);
            BinaryRW.WriteInt16(1205308, mapVariables.EarthquakeXCoordinate);
            BinaryRW.WriteInt16(1205288, mapVariables.EarthquakeYCoordinate);
            BinaryRW.WriteInt16(1205296, mapVariables.EarthquakeYCoordinate);
            BinaryRW.WriteInt16(1205304, mapVariables.EarthquakeYCoordinate);
            BinaryRW.WriteInt16(1205312, mapVariables.EarthquakeYCoordinate);


        }
    }
}
