﻿using System.Collections.ObjectModel;
using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class QuotasListViewModel : BaseViewModel
    {
        public ObservableCollection<QuotaChangesViewModel> Items { get; set; }
    }
}
