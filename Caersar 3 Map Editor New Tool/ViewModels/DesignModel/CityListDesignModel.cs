﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using static Caersar_3_Map_Editor_New_Tool.CityListItemViewModel;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class CityListDesignModel : CityListViewModel
    {
        public static CityListDesignModel Instance => new CityListDesignModel();

        public CityListDesignModel()
        {
            Items = new ObservableCollection<CityListItemViewModel>
            {
                new CityListItemViewModel
                {
                    CityName = "Byzzi1e",
                    OpenTradeCost = "8999",
                    IsSeaTradeRoute = true,
                    Wheat = new Good { CityGood = TradeGood.DefaultTradeGoodsList[1], GoodQuota = 40, IsCityBuyingGood = true },
                    Vegetables = new Good { CityGood = TradeGood.DefaultTradeGoodsList[2], GoodQuota = 40, IsCityBuyingGood = false },
                    Fruits = new Good { CityGood = TradeGood.DefaultTradeGoodsList[3], GoodQuota = 40, IsCityBuyingGood = null },
                    Olives = new Good { CityGood = TradeGood.DefaultTradeGoodsList[4], GoodQuota = 40, IsCityBuyingGood = null },
                    Vines = new Good { CityGood = TradeGood.DefaultTradeGoodsList[5], GoodQuota = 40, IsCityBuyingGood = null },
                    Meat = new Good { CityGood = TradeGood.DefaultTradeGoodsList[6], GoodQuota = 40, IsCityBuyingGood = null },
                    Wine = new Good { CityGood = TradeGood.DefaultTradeGoodsList[7], GoodQuota = 40, IsCityBuyingGood = null },
                    Oil = new Good { CityGood = TradeGood.DefaultTradeGoodsList[8], GoodQuota = 40, IsCityBuyingGood = null },
                    Iron = new Good { CityGood = TradeGood.DefaultTradeGoodsList[9], GoodQuota = 40, IsCityBuyingGood = null },
                    Timber = new Good { CityGood = TradeGood.DefaultTradeGoodsList[10], GoodQuota = 40, IsCityBuyingGood = false },
                    Clay = new Good { CityGood = TradeGood.DefaultTradeGoodsList[11], GoodQuota = 40, IsCityBuyingGood = false },
                    Marble = new Good { CityGood = TradeGood.DefaultTradeGoodsList[12], GoodQuota = 40, IsCityBuyingGood = true },
                    Weapons = new Good { CityGood = TradeGood.DefaultTradeGoodsList[13], GoodQuota = 40, IsCityBuyingGood = null },
                    Furniture = new Good { CityGood = TradeGood.DefaultTradeGoodsList[14], GoodQuota = 40, IsCityBuyingGood = null },
                    Pottery = new Good { CityGood = TradeGood.DefaultTradeGoodsList[15], GoodQuota = 40, IsCityBuyingGood = null },

                    QuotasListViewModel = new QuotasListViewModel
                    {
                        Items = new ObservableCollection<QuotaChangesViewModel>
                        {
                            new QuotaChangesViewModel
                            {
                                YearOffset = 02,
                                Month = 1,
                                TradeGoodItem = TradeGood.DefaultTradeGoodsList[1],
                                TradeRouteID = 1,
                                IsQuotaIncrease = true,
                            },
                            new QuotaChangesViewModel
                            {
                                YearOffset = 8,
                                Month = 3,
                                TradeGoodItem = TradeGood.DefaultTradeGoodsList[3],
                                TradeRouteID = 1,
                                IsQuotaIncrease = true,
                            },

                        }
                    }

        },
                new CityListItemViewModel
                {
                    CityName = "Rome",
                    OpenTradeCost = "9001",
                    IsSeaTradeRoute = true,
                    Wheat = new Good { CityGood = TradeGood.DefaultTradeGoodsList[1], GoodQuota = 40, IsCityBuyingGood = true },
                    Vegetables = new Good { CityGood = TradeGood.DefaultTradeGoodsList[2], GoodQuota = 40, IsCityBuyingGood = false },
                    Fruits = new Good { CityGood = TradeGood.DefaultTradeGoodsList[3], GoodQuota = 40, IsCityBuyingGood = null },
                    Olives = new Good { CityGood = TradeGood.DefaultTradeGoodsList[4], GoodQuota = 40, IsCityBuyingGood = null },
                    Vines = new Good { CityGood = TradeGood.DefaultTradeGoodsList[5], GoodQuota = 40, IsCityBuyingGood = null },
                    Meat = new Good { CityGood = TradeGood.DefaultTradeGoodsList[6], GoodQuota = 40, IsCityBuyingGood = null },
                    Wine = new Good { CityGood = TradeGood.DefaultTradeGoodsList[7], GoodQuota = 40, IsCityBuyingGood = null },
                    Oil = new Good { CityGood = TradeGood.DefaultTradeGoodsList[8], GoodQuota = 40, IsCityBuyingGood = null },
                    Iron = new Good { CityGood = TradeGood.DefaultTradeGoodsList[9], GoodQuota = 40, IsCityBuyingGood = null },
                    Timber = new Good { CityGood = TradeGood.DefaultTradeGoodsList[10], GoodQuota = 40, IsCityBuyingGood = false },
                    Clay = new Good { CityGood = TradeGood.DefaultTradeGoodsList[11], GoodQuota = 40, IsCityBuyingGood = false },
                    Marble = new Good { CityGood = TradeGood.DefaultTradeGoodsList[12], GoodQuota = 40, IsCityBuyingGood = true },
                    Weapons = new Good { CityGood = TradeGood.DefaultTradeGoodsList[13], GoodQuota = 40, IsCityBuyingGood = null },
                    Furniture = new Good { CityGood = TradeGood.DefaultTradeGoodsList[14], GoodQuota = 40, IsCityBuyingGood = null },
                    Pottery = new Good { CityGood = TradeGood.DefaultTradeGoodsList[15], GoodQuota = 40, IsCityBuyingGood = null },
                },  
                new CityListItemViewModel
                {
                    CityName = "Third Rome",
                    OpenTradeCost = "1",
                    IsSeaTradeRoute = false,
                    Wheat = new Good { CityGood = TradeGood.DefaultTradeGoodsList[1], GoodQuota = 40, IsCityBuyingGood = true },
                    Vegetables = new Good { CityGood = TradeGood.DefaultTradeGoodsList[2], GoodQuota = 40, IsCityBuyingGood = false },
                    Fruits = new Good { CityGood = TradeGood.DefaultTradeGoodsList[3], GoodQuota = 40, IsCityBuyingGood = null },
                    Olives = new Good { CityGood = TradeGood.DefaultTradeGoodsList[4], GoodQuota = 40, IsCityBuyingGood = null },
                    Vines = new Good { CityGood = TradeGood.DefaultTradeGoodsList[5], GoodQuota = 40, IsCityBuyingGood = null },
                    Meat = new Good { CityGood = TradeGood.DefaultTradeGoodsList[6], GoodQuota = 40, IsCityBuyingGood = null },
                    Wine = new Good { CityGood = TradeGood.DefaultTradeGoodsList[7], GoodQuota = 40, IsCityBuyingGood = null },
                    Oil = new Good { CityGood = TradeGood.DefaultTradeGoodsList[8], GoodQuota = 40, IsCityBuyingGood = null },
                    Iron = new Good { CityGood = TradeGood.DefaultTradeGoodsList[9], GoodQuota = 40, IsCityBuyingGood = null },
                    Timber = new Good { CityGood = TradeGood.DefaultTradeGoodsList[10], GoodQuota = 40, IsCityBuyingGood = false },
                    Clay = new Good { CityGood = TradeGood.DefaultTradeGoodsList[11], GoodQuota = 40, IsCityBuyingGood = false },
                    Marble = new Good { CityGood = TradeGood.DefaultTradeGoodsList[12], GoodQuota = 40, IsCityBuyingGood = true },
                    Weapons = new Good { CityGood = TradeGood.DefaultTradeGoodsList[13], GoodQuota = 40, IsCityBuyingGood = null },
                    Furniture = new Good { CityGood = TradeGood.DefaultTradeGoodsList[14], GoodQuota = 40, IsCityBuyingGood = null },
                    Pottery = new Good { CityGood = TradeGood.DefaultTradeGoodsList[15], GoodQuota = 40, IsCityBuyingGood = null },
                },
            };
        }

    }
}
