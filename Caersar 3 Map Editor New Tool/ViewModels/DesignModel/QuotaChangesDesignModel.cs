﻿using PropertyChanged;

namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]
    class QuotaChangesDesignModel : QuotaChangesViewModel
    {
        public static QuotaChangesDesignModel Instance => new QuotaChangesDesignModel();

        public QuotaChangesDesignModel()
        {
            YearOffset = 8;
            Month = 6;
            TradeGoodItem = TradeGood.DefaultTradeGoodsList[6];
            TradeRouteID = 1;
            IsQuotaIncrease = true;
        }
    }
}
