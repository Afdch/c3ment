﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]
    class QuotaListDesignModel : QuotasListViewModel
    {

        public static QuotaListDesignModel Instance => new QuotaListDesignModel();

        public QuotaListDesignModel()
        {
            Items = new System.Collections.ObjectModel.ObservableCollection<QuotaChangesViewModel>
            {
                new QuotaChangesViewModel
                {
                    YearOffset = 12,
                    Month = 6,
                    TradeGoodItem = TradeGood.DefaultTradeGoodsList[6],
                    TradeRouteID = 1,
                    IsQuotaIncrease = true,
                },
                new QuotaChangesViewModel
                {
                    YearOffset = 18,
                    Month = 3,
                    TradeGoodItem = TradeGood.DefaultTradeGoodsList[3],
                    TradeRouteID = 1,
                    IsQuotaIncrease = true,
                },
                new QuotaChangesViewModel
                {
                    YearOffset = 22,
                    Month = 2,
                    TradeGoodItem = TradeGood.DefaultTradeGoodsList[2],
                    TradeRouteID = 1,
                    IsQuotaIncrease = false,
                },
            };

        }
    }
}
