﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;


namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]

    class RatingsDesignModel : RatingsViewModel
    {

        public static RatingsDesignModel Instance => new RatingsDesignModel();

        public RatingsDesignModel()
        {
            CityName = "Roma";
            StartingDate = -120;
            GovernorName = "Yagamoth himself";

            RequiredCulture = 50;
            RequiredProsperity = 60;
            RequiredPeace = 80;
            RequiredFavor = 90;

            RequiredPopulationFlag = true;
            RequiredPopulation = 10000;

            ResqueLoan = 100;
            EarthquakeSeverity = 0;
            EarthquakeYear = 0;
            EarthquakeXCoordinate = 0xFFFF;
            EarthquakeYCoordinate = 0xFFFF;
            ChangeOfEmperorFlag = true;
            ChangeOfEmperorYear = 24;

            GladiatorRevoltFlag = true;
            GladiatorRevoltYear = 25;

            SeaTradeProblemsFlag = true;
            LandTradeProblemsFlag = true;
            RomeRaisesWagesFlag = true;
            RomeLowersWagesFlag = true;
            ContaminatedWaterFlag = true;
            IronMinesCollapseFlag = true;
            ClayPitsFloodedFlag = true;
    }
    }
}
