﻿using PropertyChanged;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace Caersar_3_Map_Editor_New_Tool
{
    [AddINotifyPropertyChangedInterface]
    public class PricesViewModel : BaseViewModel
    {
        public static RequestsListViewModel Instance => new RequestsListViewModel();

        public PricesViewModel()
        {
            RequestsList = new ObservableCollection<ImperialRequest>

            {
                new ImperialRequest
                {
                    GoodId = 10,
                    RequestAmount = 10,
                    RequestDuration = 2,
                    RequestFavor = 5,
                    RequestYear = 6

                },
                new ImperialRequest
                {
                    GoodId = 5,
                    RequestAmount = 20,
                    RequestDuration = 4,
                    RequestFavor = 10,
                    RequestYear = 12

                },
            };
        }
               
        public ObservableCollection<TradeGood> TradeGoodList { get; set; }

        public class ImperialRequest : BaseViewModel
        {
            public int GoodId { get; set; } = 0;
            public int RequestYear { get; set; } = 0;
            public int RequestDuration { get; set; } = 0;
            public int RequestAmount { get; set; } = 0;
            public int RequestFavor { get; set; } = 0;
        }

        public static ObservableCollection<ImperialRequest> GetRequestedGoods(BinaryReader br)
        {
            int requestsOffset = 1203546;
            ObservableCollection<ImperialRequest> requestedGoods = new ObservableCollection<ImperialRequest>();


            for (int i = 0; i < 20; i++)
            {
                requestedGoods.Add(new ImperialRequest()
                {
                    RequestYear = HexReadWrite.ReadHexInt(br, requestsOffset + 2 * i, 2),
                    GoodId = HexReadWrite.ReadHexInt(br, requestsOffset + 40 + 2 * i, 2),
                    RequestAmount = HexReadWrite.ReadHexInt(br, requestsOffset + 80 + 2 * i, 2),
                    RequestDuration = HexReadWrite.ReadHexInt(br, requestsOffset + 120 + 2 * i, 2),
                    RequestFavor = HexReadWrite.ReadHexInt(br, 1204880 + i, 1)
                });
            }
            return requestedGoods;
        }


        public static void SetRequestsGoods(BinaryWriter bw, ObservableCollection<ImperialRequest> requestedGoods)
        {
            int requestsOffset = 1203546;
            for (int i = 0; i < 20; i++)
            {
                HexReadWrite.WriteHexInt(bw, requestsOffset + i * 2, 2, requestedGoods[i].RequestYear);
                HexReadWrite.WriteHexInt(bw, requestsOffset + 40 + i * 2, 2, requestedGoods[i].GoodId);
                HexReadWrite.WriteHexInt(bw, requestsOffset + 80 + i * 2, 2, requestedGoods[i].RequestAmount);
                HexReadWrite.WriteHexInt(bw, requestsOffset + 120 + i * 2, 2, requestedGoods[i].RequestDuration);
                HexReadWrite.WriteHexInt(bw, 1204880 + i, 1, requestedGoods[i].RequestFavor);
            }
        }
    }
}
