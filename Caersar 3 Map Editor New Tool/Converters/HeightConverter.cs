﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class HeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int height = value != null ? System.Convert.ToInt32(value) : 0;
            return  height > 100 ? height - 100 : height;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
