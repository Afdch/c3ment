﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Caersar_3_Map_Editor_New_Tool
{
    public class OffsetYearToYearConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int offsetYear = value != null ? System.Convert.ToInt32(value) : 0;
            if (offsetYear < 0) offsetYear = 0;
            return offsetYear + MainWindow.FlagsRatings.StartingDate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();/*
            int typedYear = value != null ? System.Convert.ToInt32(value)  : 0;

                return MainWindow.FlagsRatings.StartingDate + typedYear;*/
        }
    }
}
