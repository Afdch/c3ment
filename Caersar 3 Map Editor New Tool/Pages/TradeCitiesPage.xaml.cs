﻿using System.IO;
using System.Windows.Controls;

namespace Caersar_3_Map_Editor_New_Tool
{
    /// <summary>
    /// Interaction logic for TradeCitiesPage.xaml
    /// </summary>
    public partial class TradeCitiesPage : Page
    {
        public TradeCitiesPage()
        {
            InitializeComponent();
        }

        public static void DisplayRawTradeCityData(BinaryReader br)
        {

            
            string text = "";
            for (int i = 0; i< 40; i++)
            {
                text += CityListItemViewModel.GetTradeCity(br, 1200492 + 66 * i);
            }

            text = "";
            for (int i = 0; i< 10; i++)
            {
                text += CityListItemViewModel.GetTradeQuota(br, 1251074 + 64 * i);
            }
            //TradeQuotasTextBox.Text = text;
        }
    
    }
}
